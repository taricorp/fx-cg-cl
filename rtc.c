#include <time.h>
#include "platform.h"

static clock_t rtc_base = 0;

int RTC_GetTicks() {
	clock_t delta = clock() - rtc_base;
	// To seconds, then 128 Hz blocks. Multiply first to reduce loss of precision.
	return delta * 128 / CLOCKS_PER_SEC;
}