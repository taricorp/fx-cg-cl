#include <stdio.h>
#include <stdlib.h>

#include "display.h"
#include "input.h"
#include "interface.h"
#include "platform.h"

// main() in the addin code (renamed via preprocessor when linking with this library)
extern int fxcg_main();

int main(int argc, char **argv) {
	if (argc <= 1) {
		fprintf(stderr, "IPC backing file path must be provided.\n");
		return 1;
	}

	if (interface_init(argv[1]) || display_init() || input_init()) {
		fprintf(stderr, "Core initialization failed. Aborting.\n");
		return 1;
	}

	fxcg_main();

	input_fini();
	display_fini();
	interface_fini();
	return 0;
}

void xerror(const char *s, int code) {
	if (s) {
		fprintf(stderr, "%s: %i\n", s, code);
	} else {
		fprintf(stderr, "xerror: %i\n", code);
	}
	exit(code);
}
