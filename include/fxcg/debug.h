#ifndef _FXCG_DEBUG_H
#define _FXCG_DEBUG_H

void fxcg_diag(const char *fmt, ...);

#endif