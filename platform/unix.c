#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "../platform.h"

void platform_mutex_acquire(mutex_t *mutex) {
    // Spinlock
    // GCC builtins here. Other compilers appear to support it, so should
    // be okay. Otherwise, check stdatomic.h
    while (__sync_lock_test_and_set(mutex, 1));
}

void platform_mutex_release(mutex_t *mutex) {
    __sync_lock_release(mutex);
}

void *platform_mmap(struct mmap *map) {
    void *result;
    if (map->backing == NULL) {
        // Anonymous
        result =  mmap(map->addr, map->size, PROT_READ | PROT_WRITE,
                       MAP_SHARED | MAP_ANON, -1, 0);
    } else {
        // Shared
        int fd;
        if ((fd = open(map->backing, O_RDWR)) != -1) {
            result = mmap(map->addr, map->size, PROT_READ | PROT_WRITE,
                          MAP_SHARED, fd, map->offset);
            close(fd);
        } else {
            result = NULL;
        }
    }
    if (result == MAP_FAILED)
        result = NULL;
    map->addr = result;
    return result;
}

void platform_munmap(struct mmap* map) {
    munmap(map->addr, map->size);
    memset(map, 0, sizeof(struct mmap));
}

void platform_msleep(unsigned milliseconds) {
    usleep(1000 * milliseconds);
}
