#include <malloc.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "../platform.h"

void platform_mutex_acquire(mutex_t *mutex) {
	// Spinlock
	while (InterlockedCompareExchange(mutex, 1, 0));
}

void platform_mutex_release(mutex_t *mutex) {
	InterlockedExchange(mutex, 0);
}

static void *mmap_shared(struct mmap *map) {
	HANDLE shm;
	void *result = NULL;
	// Coherence is only provided between mappings with the same tag.
	// We'll say it's always going to be "FXCGCL".
	// Fun fact: this fails with ERROR_ACCESS_DENIED if the existing mapping
	// has a different size than we request here, apparently?
	shm = OpenFileMappingA(FILE_MAP_READ | FILE_MAP_WRITE,
						   FALSE, map->backing);
	if (shm != NULL) {
		result = MapViewOfFileEx(shm,
								 FILE_MAP_ALL_ACCESS,
								 0, map->offset,
								 map->size, map->addr);
		CloseHandle(shm);
	}
	return result;
}

static void *mmap_anon(struct mmap *map) {
	return VirtualAlloc(map->addr, map->size,
						MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
}

void *platform_mmap(struct mmap *map) {
	void *result = map->backing == NULL ? mmap_anon(map) : mmap_shared(map);
	map->addr = result;
	return result;
}

void platform_munmap(struct mmap *map) {
	if (map->backing == NULL) {
		VirtualFree(map->addr, map->size, MEM_RELEASE);
	} else {
		UnmapViewOfFile(map->addr);
	}
	map->addr = NULL;
	map->offset = 0;
	map->size = 0;
}

void platform_msleep(unsigned milliseconds) {
	Sleep(milliseconds);
}
