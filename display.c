#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "include/fxcg/display.h"

#include "interface.h"
#include "platform.h"
#include "res/large_letters.h"

// Milliseconds per frame
#define FRAME_TIME (1000 / 20)

struct mmap vram_map;
/*
 * Calculator's video memory.
 *
 * User programs should get it with a call to GetVRAMAddress, but some already
 * assume it's at 0xA8000000. Thus we also try to use platform_map_alloc() to
 * put it at the same address it is at on the calculator hardware.
 */
uint16_t *VRAM;

/*
 * Utility functions to convert between 3-bit and 16-bit colors.
 */
static uint16_t C3toC16(unsigned c){
    return (c & 1 ? 0x1F : 0) | (c & 2 ? 0x7E0 : 0) | (c & 4 ? 0xF800 : 0);
}
static uint16_t C16toC3(unsigned c){
    return (c & 0x8000 ? 0xF800 : 0) | (c & 0x400 ? 0x7E0 : 0) | (c & 0x10 ? 0x1F : 0);
}

/*
 * Display coordinates for system-managed display functions.
 */
static unsigned locate_x = 1;
static unsigned locate_y = 1;
/*
 * Flag for 3-bit color mode (default) or 16-bit color mode.
 */
static uint8_t C16 = 0;

void VRAM_XORSprite(const color_t* data, int x, int y, int width, int height)
{
    color_t* V = VRAM;
    V += (LCD_WIDTH_PX * y + x);
    uint16_t i, j;
    for(j = y; j < y + height; j++) {
        for(i = x; i < x + width;  i++) {
            *(V++) ^= *(data++);
        }
        V += (LCD_WIDTH_PX - width);
    }
}

void VRAM_CopySprite(const color_t* data, int x, int y, int width, int height) {
    color_t* V = VRAM;
    V += (LCD_WIDTH_PX * y + x);
    uint16_t i, j;
    for(j = y; j < y + height; j++) {
        for(i = x; i < x + width;  i++) {
            *(V++) = *(data++);
        }
        V += (LCD_WIDTH_PX - width);
    }
}

void VRAM_CopySprite_Char(const color_t* data, int x, int y, int width, int height, char drawBack, char invert, short textColor) {
    color_t* V = VRAM;
    V += (LCD_WIDTH_PX * y + x);
    short i, j;
    for(j = y; j < y + height; j++) {
        for(i = x; i < x + width;  i++) {
            short color = *(data++);
            if(invert) {
                color = (color == 0 ? 0xffff : 0);
            }

            if((drawBack && color) || !color) {
                *(V++) = color | textColor;
            } else {
                V++;
            }
        }
        V += (LCD_WIDTH_PX - width);
    }
}

void Bdisp_EnableColor( int n ) {
    C16 = n & 1;
}

// TODO: Is mode the same as PrintXY? <-- Assuming yes
// TODO: Is zero2 ignored?
void Print_OS( unsigned char*msg, int mode, int zero2 ) {
    int i;

    if(locate_x > 22 || locate_x < 1)
        return;
    if(locate_y > 8 || locate_y < 1)
        return;

    for(i = 0; locate_x < 22 && i < strlen(msg); i++) {
        unsigned char* sprite;

        LARGE_GET(msg[i],sprite);

        if(!sprite) {
            printf("Invalid character: '%c'\n", msg[i]);
            continue; // INVALID CHARACTER
        }

        unsigned char w = *(sprite++);
        unsigned char h = *(sprite++);

        VRAM_CopySprite_Char((color_t*)sprite, (locate_x-1)*w, (locate_y-1)*h+0x24, w, h, !(mode & TEXT_MODE_TRANSPARENT_BACKGROUND), mode & TEXT_MODE_INVERT, 0);

        locate_x++;
    }
}

void Bdisp_PutDisp_DD() {
    platform_mutex_acquire(&ipc_interface->vram_mutex);
    memcpy(ipc_interface->vram, VRAM, sizeof(ipc_interface->vram));
    if(!C16) {
        int i = 0;
        for(;i < sizeof(ipc_interface->vram)/2; i++) {
            ipc_interface->vram[i] = C16toC3(ipc_interface->vram[i]);
        }
    }
    platform_mutex_release(&ipc_interface->vram_mutex);
}

void Bdisp_AllCr_VRAM() {
    memset(VRAM, 0xFF, LCD_WIDTH_PX*LCD_HEIGHT_PX*2);
}

void Bdisp_AllClr_VRAM() {
    memset(VRAM, 0xFF, LCD_WIDTH_PX*LCD_HEIGHT_PX*2);
}

void locate_OS( int X, int y ) {
    locate_x = X;
    locate_y = y;
}

void PrintXY( int x, int y, char*string, int mode, int color ) {
    int i;

    if(string[0] == 0 || string[1] == 0)
        return;
    if(x > 22 || x < 1)
        return;
    if(y > 8 || y < 1)
        return;

    for(i = 2; x < 22 && i < strlen(string); i++) {
        unsigned char* sprite;

        LARGE_GET(string[i],sprite);

        if(!sprite) {
            printf("Invalid character: '%c'\n", string[i]);
            continue; // INVALID CHARACTER
        }

        unsigned char w = *(sprite++);
        unsigned char h = *(sprite++);

        VRAM_CopySprite_Char((color_t*)sprite, (x-1)*w, (y-1)*h+0x24, w, h, !(mode & TEXT_MODE_TRANSPARENT_BACKGROUND), mode & TEXT_MODE_INVERT, C3toC16(color));

        x++;
    }
}

/*
 * Syscall that returns a pointer to system VRAM.
 */
void *GetVRAMAddress() {
    return VRAM;
}

int display_init() {
    vram_map.addr = (void *)0xA8000000;
    vram_map.backing = NULL;
    vram_map.size = 384 * 216 * 2;
    VRAM = (uint16_t *)platform_mmap(&vram_map);
    if (VRAM == NULL) {
        fprintf(stderr, "Failed to create fixed mapping for VRAM.\n");
        perror("platform_mmap");
        if (sizeof(void *) <= 4) {
            fprintf(stderr, "Fixed allocation will not usually work on 32-bit systems, which this"
                            "appears to be.\n");
        }

        if ((VRAM = (uint16_t *)calloc(2, 384 * 216)) == NULL) {
            fprintf(stderr, "Failed to allocate floating VRAM map. This shouldn't happen.\n");
            return 1;
        }
    }
    return 0;
}

void display_fini() {
    if (vram_map.addr != NULL) {
        platform_munmap(&vram_map);
    } else {
        free(VRAM);
    }
}

