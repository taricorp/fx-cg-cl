// Letter resources for the large font
#include "large/0.h"
#include "large/1.h"
#include "large/2.h"
#include "large/3.h"
#include "large/4.h"
#include "large/5.h"
#include "large/6.h"
#include "large/7.h"
#include "large/8.h"
#include "large/9.h"
#include "large/accent.h"
#include "large/amp.h"
#include "large/a.h"
#include "large/asterisk.h"
#include "large/at.h"
#include "large/b.h"
#include "large/bslash.h"
#include "large/carrot.h"
#include "large/colon.h"
#include "large/comma.h"
#include "large/c.h"
#include "large/dollar.h"
#include "large/dquote.h"
#include "large/d.h"
#include "large/equals.h"
#include "large/e.h"
#include "large/exclamation.h"
#include "large/f.h"
#include "large/fslash.h"
#include "large/g.h"
#include "large/gthan.h"
#include "large/h.h"
#include "large/i.h"
#include "large/j.h"
#include "large/k.h"
#include "large/lcbracket.h"
#include "large/lparenth.h"
#include "large/l.h"
#include "large/lsbracket.h"
#include "large/lthan.h"
#include "large/minus.h"
#include "large/m.h"
#include "large/n.h"
#include "large/o.h"
#include "large/percent.h"
#include "large/period.h"
#include "large/pipe.h"
#include "large/plus.h"
#include "large/pound.h"
#include "large/p.h"
#include "large/q.h"
#include "large/question.h"
#include "large/quote.h"
#include "large/rcbracket.h"
#include "large/rparenth.h"
#include "large/r.h"
#include "large/rsbracket.h"
#include "large/scolon.h"
#include "large/space.h"
#include "large/s.h"
#include "large/tilde.h"
#include "large/t.h"
#include "large/u_a.h"
#include "large/u_b.h"
#include "large/u_c.h"
#include "large/u_d.h"
#include "large/u_e.h"
#include "large/u_f.h"
#include "large/u_g.h"
#include "large/u_h.h"
#include "large/u_i.h"
#include "large/u_j.h"
#include "large/u_k.h"
#include "large/u_l.h"
#include "large/u_m.h"
#include "large/underscore.h"
#include "large/u_n.h"
#include "large/u_o.h"
#include "large/u_p.h"
#include "large/u_q.h"
#include "large/u.h"
#include "large/u_r.h"
#include "large/u_s.h"
#include "large/u_t.h"
#include "large/u_u.h"
#include "large/u_v.h"
#include "large/u_w.h"
#include "large/u_x.h"
#include "large/u_y.h"
#include "large/u_z.h"
#include "large/v.h"
#include "large/w.h"
#include "large/x.h"
#include "large/y.h"
#include "large/z.h"
struct {char letter; unsigned char* sprite; } large_map[] = {
{'0',large_0},
{'1',large_1},
{'2',large_2},
{'3',large_3},
{'4',large_4},
{'5',large_5},
{'6',large_6},
{'7',large_7},
{'8',large_8},
{'9',large_9},
{'`',large_accent},
{'&',large_amp},
{'a',large_a},
{'*',large_asterisk},
{'@',large_at},
{'b',large_b},
{'\\',large_bslash},
{'^',large_carrot},
{':',large_colon},
{',',large_comma},
{'c',large_c},
{'$',large_dollar},
{'"',large_dquote},
{'d',large_d},
{'=',large_equals},
{'e',large_e},
{'!',large_exclamation},
{'f',large_f},
{'/',large_fslash},
{'g',large_g},
{'>',large_gthan},
{'h',large_h},
{'i',large_i},
{'j',large_j},
{'k',large_k},
{'{',large_lcbracket},
{'(',large_lparenth},
{'l',large_l},
{'[',large_lsbracket},
{'<',large_lthan},
{'-',large_minus},
{'m',large_m},
{'n',large_n},
{'o',large_o},
{'%',large_percent},
{'.',large_period},
{'|',large_pipe},
{'+',large_plus},
{'#',large_pound},
{'p',large_p},
{'q',large_q},
{'?',large_question},
{'\'',large_quote},
{'}',large_rcbracket},
{')',large_rparenth},
{'r',large_r},
{']',large_rsbracket},
{';',large_scolon},
{' ',large_space},
{'s',large_s},
{'~',large_tilde},
{'t',large_t},
{'A',large_u_a},
{'B',large_u_b},
{'C',large_u_c},
{'D',large_u_d},
{'E',large_u_e},
{'F',large_u_f},
{'G',large_u_g},
{'H',large_u_h},
{'I',large_u_i},
{'J',large_u_j},
{'K',large_u_k},
{'L',large_u_l},
{'M',large_u_m},
{'_',large_underscore},
{'N',large_u_n},
{'O',large_u_o},
{'P',large_u_p},
{'Q',large_u_q},
{'u',large_u},
{'R',large_u_r},
{'S',large_u_s},
{'T',large_u_t},
{'U',large_u_u},
{'V',large_u_v},
{'W',large_u_w},
{'X',large_u_x},
{'Y',large_u_y},
{'Z',large_u_z},
{'v',large_v},
{'w',large_w},
{'x',large_x},
{'y',large_y},
{'z',large_z},
};
#define LARGE_GET(c,sprite) {sprite=0;int largei;for(largei=0;largei<sizeof(large_map)/(sizeof(unsigned char*)+sizeof(char));largei++){if(large_map[largei].letter==(c)){sprite=large_map[largei].sprite;break;}}}
