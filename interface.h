#include <stdint.h>
#include "platform.h"

int interface_init(const char *file);
void interface_fini();

/*
 * Calls platform_mmap after setting the backing store to the current interface
 * backing item.
 * Just use platform_munmap to free the resultant mapping.
 */
void *interface_map(struct mmap *info);

struct ipc_interface {
	// System keyboard registers. They come first because we want to
	// interface_map them at the right place, and most systems have a large
	// minimum granularity for the offset (64k or so).
	// No mutex here since partial updates are not harmful.
	uint16_t keyboard_regs[8];
	// IPC VRAM. This gets displayed at regular intervals.
	uint16_t vram[384 * 216];
	// Control access to IPC VRAM to avoid tearing from partial updates.
	mutex_t vram_mutex;
};
/*
 * Shared memory mapping connected to the user interface.
 */
extern struct ipc_interface *ipc_interface;