#include <stdio.h>
#include "interface.h"
#include "platform.h"

static struct mmap ipc_map;
struct ipc_interface *ipc_interface = NULL;

int interface_init(const char *file) {
	ipc_map.backing = file;
	ipc_map.offset = 0;
	ipc_map.size = sizeof(struct ipc_interface);
	ipc_map.addr = NULL;
	if ((ipc_interface = (struct ipc_interface *)interface_map(&ipc_map)) == NULL) {
        perror("interface_map");
		return 1;
	}
	// Ensure we won't deadlock if something died with the lock held.
	ipc_interface->vram_mutex = MUTEX_UNLOCKED;
	return 0;
}

void interface_fini() {
	platform_munmap(&ipc_map);
}

void *interface_map(struct mmap *info) {
	info->backing = ipc_map.backing;
	return platform_mmap(info);
}
