#include <stddef.h>
#include <stdio.h>
#include "interface.h"

static struct mmap kbd_mmap;
static void *kbd_regs = NULL;

int input_init() {
	kbd_mmap.addr = (void *)0xA44B0000;
	kbd_mmap.offset = 0;
	kbd_mmap.size = sizeof(ipc_interface->keyboard_regs);
	if (interface_map(&kbd_mmap) == NULL) {
		fprintf(stderr, "Failed to create fixed mapping for keyboard registers.\n"
						"Things may go bad very quickly.\n");

		kbd_mmap.addr = NULL;
		if (interface_map(&kbd_mmap) == NULL) {
			fprintf(stderr, "Floating map for keyboard failed too.. something's wrong.\n"
							"Bailing out.\n");
			return 1;
		}
	}
	return 0;
}

void input_fini() {
	platform_munmap(&kbd_mmap);
}