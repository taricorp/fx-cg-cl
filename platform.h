#ifndef _PLATFORM_H
#define _PLATFORM_H

/*
 * Like perror, but doesn't translate the error code.
 */
void xerror(const char *s, int code);

typedef unsigned int mutex_t ;
#define MUTEX_UNLOCKED 0
void platform_mutex_acquire(mutex_t *mutex);
void platform_mutex_release(mutex_t *mutex);

/*
 * Bookkeeping structure for memory mappings.
 */
struct mmap {
    // Backing item, or NULL for anonymous mappings
    const char *backing;
    // Offset in backing item. Ignored if backing == NULL
    size_t offset;
    // Size (bytes)
    size_t size;
    // Base address
    void *addr;
};

/*
 * Open a memory mapping.
 *
 * If backing is non-NULL, opens a shared mapping backed by the object
 * identified by that string. Otherwise the mapping is anonymous.
 *
 * If the base address is non-NULL, will attempt to place the mapped region
 * at the given address.
 *
 * Writes the address of the created mapping into the input structure in
 * addition to returning it. Returns NULL on failure.
 */
void *platform_mmap(struct mmap *map);
/*
 * Unmap a memory mapping.
 * Must have been successfully mapped with platform_mmap.
 */
void platform_munmap(struct mmap *map);

/*
 * Sleep for at least the given amount of time (in milliseconds).
 */
void platform_msleep(unsigned milliseconds);

#endif
